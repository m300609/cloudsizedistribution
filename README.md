# Cloud Size Distributions

## Model Description

The key idea is to show that a double exponential cloud size distribution can arise from a single exponential thermal size distribution. The model proceeds in two main steps. First, merged thermals are created from a single exponential distribution. Second, clouds are generated from the resulting distribution of merged thermals. 

### Merging Thermals Model

Initialize 1D model (tm) with length *L_domain* (100km) and all values set to zero, tm(*x*)=0 for x in [0, *L_domain*]. A spatial resolution of 1m is used.

1. Draw thermal size *s_1* from an exponential distribution (mean is *L* = 100m) and thermal location *x_1* from either a uniform (no clustering) or normal distribution (clustering strength increases with decreasing standard deviation). Set the model to one (tm(*x*)=1) for *x* within [*x_1*, *x_1*+*s_1*). 
2. Calculate the coverage fraction [*p*=sum(tm(x))/L]
3. If coverage fraction *p* < *p_thermal*, go back to 1.

Calculate the merged thermal size distribution from tm(*x*) by treating all connected regions of tm(*x*)=1 as (potentially merged) thermals. 

An example of thermal size distributions for different clustering strengths is shown below, showing how the distribution shifts toward larger thermal sizes (second exponential mode appears) for stronger clustering.

![](https://pad.gwdg.de/uploads/0e1a70e9-e35a-4c8a-88f8-400f7a257b73.png)

Note: The clustering strength in the legend is given as the standard deviation of the normal distribution divided by *L_domain*.

### Making clouds

The cloud model is largely a reiteration of the merged thermal model described above, with two key differences:

* Since not all thermals turn into clouds, reduce the cloud fraction compared to the merged thermal fraction (divided by 5)
* Since not the entire cloud base is covered by a thermal, multiply thermals by a factor (times 1.5)

An example of the resulting cloud size distributions based on the different thermal sizes shown above is shown below. 

![](https://pad.gwdg.de/uploads/6892f666-e52a-4c6a-b2f6-84e257c8b2ff.png)

